/// <reference types="Cypress" />

describe('Visualize Feature/Models usages', function() {
    it('Visit Features Web Page', function() {
        // Wait for the route aliased as 'getFilters' to respond
        cy.server();
        cy.route('/features/filters?').as('getFilters');
        cy.visit('/'); // The base URL is defined in the cypress jason file
        cy.wait('@getFilters').its('status').should('eq', 200).then((xhr) => {
            // we can now access the low level xhr
            // that contains the request body,
            // response body, status, etc
            console.log(xhr.response);
          })
        cy.root().click(600,600);
        cy.get('.search-button').click();
    })

    it('Check if there are filters', function(){
        // Product Type filters
        cy.contains('Product Type')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .first()
        .should('contain','CAR')
        //.should('have.css','ng-reflect-checked');

        // Brand filters
        cy.contains('Brand')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .first()
        .should('contain','BMWi');

        // Market filters
        cy.contains('Market')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .first()
        .should('contain','AG');
    })

    it('Check if the default filters are selected', function(){
        // Product Type filters
        cy.contains('Product Type')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .contains('CAR')
        .parent()
        .should('have.class','mat-radio-checked');

        // Brand filters
        cy.contains('Brand')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .contains('BMW ')
        .parent()
        .should('have.class','mat-radio-checked');

        // Market filters
        cy.contains('Market')
        .siblings('mat-radio-group')
        .find('mat-radio-button')
        .contains('GERMANY')
        .parent()
        .should('have.class','mat-radio-checked');
    })

    it('Select Dev Series "F45"', function(){
        cy.server();
        cy.route('/features?**').as('getFeatures');

        cy.contains('Dev Series')
        .siblings('mat-checkbox')
        .contains('F45')
        .click();

        // Wait for the route aliased as 'getFeatures' to respond
        cy.wait('@getFeatures').its('status').should('eq', 200).then((xhr) => {
            // we can now access the low level xhr
            // that contains the request body,
            // response body, status, etc
            console.log(xhr);
            })
    })
    /*
    it('Check if the data shown is valid', function(){
        cy.get(':nth-child(3) > .assignments > .serie > :nth-child(6) > .value > svg')
        .should(contain, "X");
    })
    */
})