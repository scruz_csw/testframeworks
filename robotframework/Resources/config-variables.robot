*** Variables ***
${HEIGHT}							1080
${WIDTH}							1920

${IP}								sprint-dev-ui.mp-prod-cnap.bmwgroup.net/features
${PORT}								80

${SPEED}							0.25
${TIMEOUT}							15

${BROWSER}							gc
${SERVER}							http://${IP}:${PORT}
${GRID_HUB}							http://10.0.75.1:4444/wd/hub
