*** Keywords ***
Open Search Panel
	Sleep  5s
	${xClick}  ${yClick}=  Get Page Coordinates  0.5  0.5
    Click Element At Coordinates  tag=body  ${xClick}  ${yClick}
	Wait Until Element Is Visible  css=div.search-button
	Element Should Be Visible  css=div.search-button
	Click Element  css=div.search-button
	
Get Page Coordinates
    [Arguments]  ${xPercent}  ${yPercent}
	${width}  ${height}=  Get Window Size
	${xCenterPage}=  Evaluate  ${width} * ${xPercent}
	${yCenterPage}=  Evaluate  ${height} * ${yPercent} 
	[Return]  ${xCenterPage}  ${yCenterPage}