*** Settings ***
Documentation  Feature test suite
Library  Selenium2Library
Resource  ../Resources/keywords.robot
Resource  ../Resources/config-variables.robot
Resource  ../Resources/gui-variables.robot
Resource  Resources/keywords.robot
Resource  Resources/config-variables.robot
Resource  Resources/gui-variables.robot
Test Teardown  Close All Browsers


*** Test Cases ***
Test filters options in the page
    [Documentation]  The filters options should be shown in the page
	[Tags]  Smoke
	Fix URL
    Open Browser  ${SERVER}  ${BROWSER}  None  ${GRID_HUB}
	Open Search Panel
	Log To Console  %{username} ran this test on %{os}
	
*** comment ***
	Open Browser  ${SERVER}  ${BROWSER}  None  ${GRID_HUB}